import React from 'react'

const About = () => {
    return (
        <div>
           <h1>About this app</h1>
           <p className = 'mt-1'>
               this is full stack react app fro keeping your contacts.
           </p>
           <p className="bg-dark p">
               <strong>Version: </strong>1.0.0
           </p>
        </div>
    )
}

export default About
